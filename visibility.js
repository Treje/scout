class Visible{
    //@TODO 1. calculate visible elements on page
    //2. calculate viewport
    //3. calculate %percentage of visible elements is on viewport

    constructor(){
        
        this.el = {}

        this.value = false 
        this.result = ""
        this.method = ""
        this.percent = {}

        this.scrollY = 0
        this.scrollX = 0

        this.style = {}
        this.boundingBox = {}

        this.trackScroll(this.track)
    }
    
    track(el){
        if(el === undefined){
            this.value = false
            this.result = "no element"
            this.method = "no element"
            return this.isVisible()
        }
        this.el = el
        this.style = window.getComputedStyle(this.el)
        this.boundingBox = this.el.getBoundingClientRect()
        this
        .isParentInvisible()
        .checkDisplay()
        .checkOpacity()
        .checkVisibility()
        .checkzIndex()
        .checkViewport()
        

        return this.isVisible()
    }
    isOnViewport(func){
       
    }
    trackScroll(func){
        document.addEventListener("scroll",(event)=>{
            this.scrollY = window.scrollY
            this.scrollX = window.scrollX
            func(this.el)
            
        })
    }
   
    isParentInvisible(el){
        if(el === undefined || el === null || el.parentElement === undefined ){
            return this
        }
        if(el.parentElement instanceof Element){
            
            this.style = window.getComputedStyle(el.parentElement)
            this
            .checkDisplay()
            .checkOpacity()
            .checkVisibility()
            .checkzIndex()
        }
       
        if(el.parentElement !== undefined)
           this.isParentInvisible(el.parentElement)

        
        this.style = window.getComputedStyle(el)
        return this;
    }
    setDefault(){
        this.value = true,
        this.result = "visible",
        this.method = "element is in viewport"
    }
    checkViewport(scrollX,scrollY){
        if(!(this.isOnXAxis(this.boundingBox.x,this.boundingBox.width) && this.isOnYAxis(this.boundingBox.y,this.boundingBox.height)) ){
           
                this.value = false,
                this.result = "invisible",
                this.method = "element is not in viewport"
            return this
        }


        let area = this.isBoxOnViewport(this.boundingBox)
        this.percentVisible = ((area.visibleArea / area.totalArea).toPrecision(2)*100) + '%'
        this.percentOfViewport = ((area.visibleArea / (window.innerWidth*window.innerHeight)).toPrecision(2)*100) + '%'
        
        try{

        
        let height = Math.max([document.body.scrollHeight,document.documentElement.scrollHeight])
        let width = Math.max([document.body.scrollWidth,document.documentElement.scrollWidth])
        this.percentOfBody = ((area.totalArea / (width * height)).toPrecision(2)*100) + '%'
        //TODO check z-axis
        }catch(e){
            console.log(e)
        }
        this.setDefault()
        this.percent = {
            visible : this.percentVisible,
            viewport: this.percentOfViewport,
            page: this.percentOfBody
        }
       
        
        return this
    }
     //CSS METHODS
     checkOpacity(){
        if(this.style.opacity === 0){
                this.value = !this.value ? false : false,
                this.result = "invisible",
                this.method ="css property: opacity is set to 0"
        }
        return this
    }
    checkDisplay(){
        if(this.style.display === "none"){ 
                this.value = !this.value ? false : false,
                this.result = "invisible",
                this.method = "css property: display is set to none"
        }
        return this
    }
    checkVisibility(){
        if(this.style.visibility === "hidden"){
                this.value = !this.value ? false : false,
                this.result = "invisible",
                this.method = "css property: visibility is set to hidden"
        }
        return this
    }
    checkzIndex(){
        if(this.style.zIndex < 0 ){
                this.value = !this.value ? false : false,
                this.result = "invisible"
                this.method =`css property: z-index is set to ${this.style.zIndex}`
        }
        return this

    }
    //TODO
    checkOverlapping(el){
        let boundingBox = el.getBoundingClientRect()
        let visibleEls = []
        Array.from(document.querySelectorAll('*')).forEach(others => {
               if(!invisible(others).value ){
                   visibleEls.push(others)
               }
          })
          let overlapping = []
            visibleEls.forEach(a => {
             let box =  a.getBoundingClientRect()
             if(el.zIndex < a.zIndex){
                overlapping.push(a);
               // boundingBox.top < box.top
               // boundingBox.left < box.left
               // boundingBox.right < box.right
               // boundingBox.bottom < box.bottom
             }
    
          })
          return overlapping
    }
    //Helpers
    isOnYAxis(y,height){
        return ((y >= 0 && y <= window.innerHeight)||(y + height > 0))
    }
    isOnXAxis(x,width){
        return ((x >= 0 && x <= window.innerWidth)||(x + width > 0))
    }
    isBoxOnViewport(box){
        let area = (w,h) => (w*h)
        let visibleHeight = (top,bottom) => {
            if(top <= 0) {top = 0}
            if(bottom <= 0) {bottom = 0}
            if(bottom >= this.viewport().height)
            {
                bottom = this.viewport().height
            }
            return Math.abs(top - bottom)
        }
        let visibleWidth = (left,right) => {
            if(left <= 0) {
                left = 0
            }
            
            if(right <= 0) {
                right = 0
            }

            if(right >= this.viewport().width){
                right = this.viewport().width
            }

            return Math.abs(left - right)
        }
        
        let totalArea = area(box.width,box.height)
        let visibleArea = area(visibleWidth(box.left,box.right),visibleHeight(box.top,box.bottom))
        
        return {
            visibleArea:visibleArea,
            totalArea:totalArea
        }
    }
    viewport(scrollX = window.scrollX,scrollY = window.scrollY){
        
        return {
            area: (window.innerWidth + scrollX) * (window.innerHeight + scrollY),
            width: window.innerWidth + scrollX,
            height: window.innerHeight + scrollY,
        }
    }
    //get results
    isVisible(){

        if(this.value){
            return{
               visible: this.value,
               visibleText: this.result,
               reason: this.method,
               percent: this.percent
            }
        }

        return {
            visible: this.value,
            visibleText: this.result,
            reason: this.method 
        }
     }

    static mainContent(){
        let threshold = 30;
        let elems = {
            viewport: [],
            page : []
        }
        let removeHeader = () => {
            let header = document.querySelector('header') || Array.from(document.querySelectorAll("*[id$='header'],header,*[class$='header']"))
            if(Array.isArray(header)){
                header.forEach(a => a.parentNode.removeChild(a))
                return;
            }
            header.parentNode.removeChild(header)

        }
        let removeFooter = () => {
            let footer = document.querySelector('footer') || Array.from(document.querySelectorAll("*[id$='footer'],footer,*[class$='footer']"))
            if(Array.isArray(footer)){
                footer.forEach(a => a.parentNode.removeChild(a))
                return;
            }
            footer.parentNode.removeChild(footer)
        }
        try{
            removeHeader()
        }catch(e){

        }
        try{
            removeFooter()
        }catch(e){

        }
        
        Array.from(document.querySelectorAll('*')).forEach(a =>  {
            let visibility = new Visible(a).isVisible()
            if(visibility.visible){
               
                if(parseInt(visibility.percent.page,10)> threshold){
                    if(parseInt(visibility.percent.viewport,10) > threshold){
                        elems['page'].push(a)
                    }
                }
               
                
            }
        })
        console.log(elems)
        return elems['page'].pop()
    }
}

module.exports = Visible
