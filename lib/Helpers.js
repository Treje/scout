let fs = require('fs');
/**
 * @func addMapLimit - adds Array.mapLimit() to Array.prototype
 *
 */
addMapLimit = () => {
    if(!('mapLimit' in Array.prototype)){
            /**@name mapLimit
                * calls a defined callback function on each element of an array, and returns an array that contains the results.
            * @param {function} func  callback function
            * @param {int} limit  number of elements to limit each map call to callback function
            *@returns {Promise<Array>}
            */
        Object.defineProperty(Array.prototype, 'mapLimit', {
            /**@name mapLimit
                * calls a defined callback function on each element of an array, and returns an array that contains the results.
            * @param {function} func  callback function
            * @param {int} limit  number of elements to limit each map call to callback function
            *@returns {Promise<Array>}
            */
            async value(func,limit,...args){
                if(limit === undefined || limit === 0){
                    limit = this.length
                    return await Promise.all(this.map(async (e,...args) => await func.call(this,e,...args)))
                }
                let result = []
                let reflect = async (promise)=>{
                   return await promise.then(result => result,error => errorValue)
                }

                let running = []
                let complete = []
                let index = 0
                let exec = async (current,length,...args) => {
                    //console.log("Current",current)
                    await Promise.all(current.map(async (data,...args) => {
                        if(!data.started){
                            data.started = true
                            let asyncData =   await func.call(this,data.index,...args)
                          
                            //.then(async asyncData => {
                                
                            addToCompletedResult(asyncData)
                            removeFromRunning(data)
                            addToFinished(data)
                            enqueue()
                            if(complete.length < length){
                                await exec(running,length)
                            }
                           // }) 
                        }
                       
                    }
                ))
                   
            }

                 let addToCompletedResult = (data) => {
                      
                        result.push(data)
                 }
                 let addToFinished = (data) => {
                    complete.push(data)
                 }
                  let removeFromRunning = (data) => {
                    //let remove = running.indexOf(running.find(a => (a.started && a.index==data)))
                    //console.log("Before",running,running.length)
                    //running.splice(remove,1)
                   
                    running = running.filter(a =>{
                         
                        //console.log("Object Comparison",Object.is(a.index,data))
                        //console.log("=== comparison",a.index !== data)
                        return (a!==undefined && (!Object.is(a,data) || a !== data) )

                    })
                    //console.log("removing",data)
                    //console.log("After",running,running.length)
                    //console.log("Result",result)
                 }

                 let enqueue = () => {
                        while(running.length < limit && index < this.length){
                            running.push({index: this[index],started:false})
                            index++
                        }
                        return index;
                 }

                enqueue()
                await exec(running,this.length)
               
                
                
                
                let begin = 0
                let end = limit
                let batch = this.slice(begin,end)
               /* while(batch.length > 0){
                    result = result.concat(await Promise.all(batch.map(func)))
                    
                    begin += limit
                    end += limit
                    batch = this.slice(begin,end)
                }*/
              return Promise.resolve(result);
            }
        });
    }
}
addUnique = () => {
    if(!('unique' in Array.prototype)){
      
        Object.defineProperty(Array.prototype, 'unique', {
        /**@name unique
       */
            value(){
               return Array.from(new Set(this))
            }
        })
    }
  
}
readFile = (filepath, type='json') => {
    return new Promise((resolve,reject) => { 
        fs.readFile(filepath,(err,data) => {
            if(err){
                return reject(err)
            }
            if(type == 'json')
                return resolve(JSON.parse(data));
            return resolve(data)
        })  
    })
}

module.exports = {
    mapLimit: addMapLimit(),
    unique: addUnique(),
    readFile,
}

