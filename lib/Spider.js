const { unique, mapLimit, readFile } = require("./Helpers");
let Scout = require("./Scout")
const Page = require("puppeteer/lib/Page");
let fs = require("fs")


/**
 * @class Spider
 * @classdesc Spider for Scout
 * @borrows Scout as scout
 */
class Spider{
    /**
     * @constructor Spider
     * @param {Object<>} config
     * @param {String}  config.name
     * @param {!Scout}  config.scout
     * Create new spider with name and pass in the scout object
     */
    constructor(config={
        name: "Temp",
        scout: null
    }){
        /**
         * @member
         *@property {string} this.name
         */
        this.name =  config.name || "Temp"
        //scout instance
        /**
         *@property {Scout} this.scout
         */
        this.scout = config.scout || new Scout()
        /**
         * @type {boolean}
         * @description Browser running headless or headfull
         */
        this.headless = false

        //command list
        /**
         * @type {Array<Object<!Scout.function,string>>}
         * 
         */
        this.linkCommands = []
        /**
         * dataCommands
         * @type {{labels:Array,selectors:Array}}
         * @description calls Scout.data() function to query the page
         * @example <caption>Example value</caption>
         * {
         *      labels:[
         *          "title"
         *      ]
         *      selectors:[
         *          ".title"
         *      ]
         * }
         */
        this.dataCommands = {}
        /**
         * @type {{_fieldname:Array<{_operation:string|{}}>}}
         */
        this.sanitizeCommands = {}

         //storage
         /**
          * @type {Array<{href:string,title:string}>}
          */
         this.links = []
         this.data = []

         
        //extra
        this.website = config.website || "" 
    
       
    }
    static async create(config){
        let spider = new Spider() 
        await spider.init(config)
        return spider;
     }
    static async createFromFile(filename){
        let config = await readFile(filename)
        return await Spider.create(config)
    }
    static async createFromDirectory(path){
        if(path === undefined){
            return new Error('No path');
        }
        let configs = fs.readdirSync(path)  
        let crawlers = []  
        //load crawler configs
            for (let name of configs) {
                //blocking operating
                let json_config = {}
                try{
    
                   json_config = JSON.parse(fs.readFileSync(path + name))
                    
                }catch(e){
                    console.log(name," ",e.message)
                    continue;
                }
               
                const { disabled } = json_config;
                if(disabled){
                    //skip
                    continue;
                }
                crawlers.push(await Spider.create(json_config)) 
            }
            return crawlers
    }   
    /**
     * @param {Object<>} config
     * @param {string} config.name - Name of the spider
     * @param {Array<{command:string|{}}>} config.links
     * @param {Array} config.data
     * @param {Array<Object>} config.sanitize
     */
    async init(config={}){
        //map json config to spider class
        Object.keys(config).forEach(key => {
            if(typeof(config[key]) === "object"){
                 //rename to avoid conflicts
                let label = (key === "links") ? "link" : key
                 this[`${label}Commands`] = config[key]
            }else{
                 this[key] = config[key]
            }  
        })

        if(this.scout === undefined){
            throw new Error("scout is null")
            this.scout = new Scout()
        }
        if(!this.scout.launched){
            await this.scout.init({headless:this.headless})
        }
        this.onCancel()
        return this;
    }
    /**
     * 
     * @param {Page} page 
     * @param {*} options 
     */
    async getLinks(page,options){
        let links = []
        if(typeof(options) === "string"){
            links = await this.scout.getLinks(page,options)
            return links
        }
        
        let selector = 'selector' in options? options.selector:""
        let next = 'click' in options? options.click: ""
        
        if(typeof (options) === "object"){
            //console.log("paginate"+ next)
            //console.log(options)

            links = await this.scout.getPaginatedLinks(page,selector,next,links)
        }
        
        return links;
    }
    /**
     * 
     * @param {Page} page 
     * @param {*} options 
     */
    async paginate(page,options){
       
        let update = (report) => {
            this.links = report.collector.links
            this.data = report.collector.data
            if(report.links){
                console.log(report.links)
            }
            if(report.data){
                console.log(report.data)
            }
            if(report.visited){
                console.log(report.visited)
            }
               
        }
        options.callback = update.bind(this)
        //object destructuring
        let collector = await this.scout.paginate(page,options)
        let links = collector.links
        let data = collector.data
        //add data to link array
        links = links.map((a,i) => {
            a.data = data[i]
            return a;
        })
        return links
    }
    /**
     * Used when collecting data from multiple pages
     * @param {Page} page 
     * @param {string|{labels:[],selectors:[]}} query 
     */
    async queryPage(page,query){
        if(typeof(query) === "string"){
             let result = await this.scout.data(page,{labels:["data"],selectors:[query]})
             if(result){
                 return result["data"]
             }
             return result
        }else{
            return await this.scout.data(page,query)
        }
    }
    /**
     * @returns {Spider}
     */
    async fetch(){
       
        console.time("Link Gathering Phase")
        let extra = {}
        let page = await this.scout.newPage()
        for(let action of this.linkCommands){
            for(let command of Object.keys(action)){
                try{
                    switch(command){
                        case "links":
                            this.links = this.links.concat(await this.getLinks(page,action[command]))
                        break;
                        case "data":
                            this.data = await this.queryPage(page,action[command]) 
                        break;
                        case 'format':
                            this.formatData();
                        break;
                        case "paginate":
                                this.links = await this.paginate(page,action[command])
                        break;
                        case "extradata":
                            extra = await this.queryPage(page,action[command])
                            
                            extra = this.formatData({data:extra})
                            
                            //add data to link array
                            this.links = this.links.map((a,i) => {
                                a.extra = extra[i]
                                return a;
                            }) 
                        break;
                        case "filter":
                           // this.links = this.links.filter(link => link action[command])
                        break;
                        case "save":
                            await this.save(action[command])
                        break;
                        default: 
                            await this.scout[command](page,action[command]) 
                        break
                    }
                }catch(e){
                    console.log(e.message)
                    if(e.message.includes('exit'))
                    {
                        console.log('Link gathering failed: '+e.message)
                        await page.close()
                        return;
                    }
                    
                    console.log(`${command} not found/failed`)
                }
            }
        }

        await page.close()
        console.timeEnd("Link Gathering Phase")
        return this
    }
    /**
     * Executes the commands in dataCommands on each item in links Array
     * @returns {Spider}
     */
    async crawl(){
        console.time(this.name)
        if(this.links === 0 ){
            return;
        }
        console.log(this.links.length)
        let data = {}
        let limit = 5
        let isJS = (/** @type {{href:string,title:string}}*/link) => {return /(^javascript)|(^js)/.test(link)}
        //let isMoreThanLimit = (value,limit) => {value>limit}
        //let setLimit = (length,limit) => {isMoreThanLimit(length,limit)?limit:0}    
        //limit = setLimit(this.links.length,limit)
       
        if(this.dataCommands.limit){
            limit = this.dataCommands.limit
        }
        await this.links.mapLimit(async link => {
          

            let page = await this.scout.newPage()
            
                if(isJS(link.href)){
                    await this.scout.goto(page,this.linkCommands[0].goto)
                    //let wait = this.linkCommands.filter(a => a.wait!==undefined)[0].wait
                   /* if(wait){
                        await page.waitFor(wait)
                    }*/
                        await page.evaluate(link.href)
                        await page.waitFor(5*1000)
    
                }else{ 
                     await this.scout.goto(page,link.href)
                }

               
            try{ 

                data = await this.scout.data(page,{labels:this.dataCommands.labels,selectors:this.dataCommands.selectors})
                
               
                if(data === undefined){
                    this.scout.failed.push(link.href)
                    return;
                }
                
               
                
                //TODO - FIX THIS
                if(Array.isArray(data)){
                    console.log(data)
                    console.log(this.name+` fetched an array from ${link.href}. Returning first element only`)
                    data =  data.filter(e => e.description != undefined).pop()
                    console.log(data)
                }
                if(link.extra){
                    Object.keys(link.extra).map(a => {
                        data[a] = link.extra[a]
                    })
                }
                if(isJS(link.href)){
                    
                    data.url = this.linkCommands[0].goto
                    data.js = link.href
                }else{
                    data.url = link.href
                }
               
            }catch(e){
                    console.log(this.name+" "+e.message)
            }  

                //TODO - WHAT IS THIS
                    if(Array.isArray(this.data))
                        this.data.push(data)
                    else{
                        Object.keys(data).map(prop =>{
                            this.data[prop] = data[prop]
                        })
                    }
                  
            await page.close()

        },limit)
      
        console.timeEnd(this.name)
        return this
    }

    isObjectEmpty(object){
        for(var key in object) {
            if(object.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    cleanDefault(data){
        try{
            if(data !== undefined && data.title !== undefined && typeof(data.title) === "string"){
                data.title = data.title.replace("\t"," ")
                data.title = data.title.trim()
            }
            
            if(data.description !== undefined && typeof(data.description) === "string"){
                data.description = data.description.split('\n').filter(a => a.trim()!=="")     
            }
            //handle accordion/text on one page job sites
            data.url = this.isUndefined(data.url,this.linkCommands[0].goto)
            return data
        }catch(e){
            console.log("WTF cleaning", e)
            return data
        }
      
    }
 
    sanitize(){
        if(this.sanitizeCommands === undefined || this.isObjectEmpty(this.sanitizeCommands)){
           if(Array.isArray(this.data)){
                this.data = this.data.map(data => {
                    data = this.cleanDefault(data)
                    return data;
                })
           }else{
               //should be an object
               this.data = this.cleanDefault(this.data)
           }
            
            return this;
        }

    this.data = this.data.map(data => {       
            let old = {}
            let newP = {}
            for(let property of Object.keys(this.sanitizeCommands)){
                //copy current propert value to temp object
                old[property] = data[property]
                //property to modify
                this.sanitizeCommands[property].map(operation => {
                    //command to use on property
                    Object.keys(operation).map( a =>{
                        //operations to perform
                        if(a == "extract"){
                            operation[a].map(b =>{ 
                                //console.log(operation)
                            
                                //get first matching group 
                                let matches = new RegExp(b.regex).exec(old[property]);
                                if(matches[1]){
                                    newP[b.name] = matches[1];
                                    console.debug(a +" : "+ b.english + " from " + property + " and save as "+ b.name)
                                    console.debug(newP[b.name])
                                    throw Error("You forgot to test my change in jobscout to fix the error: Cannot readt property 1 of null");
                                }
                               
                            })
                        }

                        if(a == "replace"){
                            //type checking
                            if(typeof(old[property]) === "string"){
                                data[property] = old[property][a](operation[a],"")
                            }
                            //find line with property
                            if(Array.isArray(old[property])){
                                let found = old[property].find(value => value.includes(operation[a]))
                                if(found){
                                    data[property] = found[a](operation[a],"")
                                }
                            }
                        }
                    })
                })
            }
            //update data
            Object.keys(newP).map(key => {
                data[key] = newP[key]
            })
            Object.keys(data).map(key=>{
                data[key] === undefined? data[key] = "N/A": ""
                if(typeof(data[key]) === "string"){
                    data[key] = data[key].trim()
                }
            })
          /*  Object.keys(data).map(key =>{
                if(key.includes('date')||key.includes("deadline")){
                    data[key] = this.scout.extractDate(data[key])
                }
            })*/
            
            
            
            data = this.cleanDefault(data)
            
            return data;
        })
       return this
    }
    isUndefined(query,new_value){
        if(query === undefined && new_value ===undefined){
            return "N/A"
        }
        if(query === undefined){
            return new_value
        }
        return query
    }
    async run(){
        try{
            await this.fetch()
        }catch(e){
            console.log("Error Fetching Links:",e)
        }
        try{
            await this.crawl()
        }catch(e){
            console.log("Error Crawling Links:",e)
        }
        try{
            await this.sanitize()
        }catch(e){
            console.log("Error Cleaning Data:",e)
        }
        try{
            await this.save(this.name)
        }catch(e){
            console.log("Error Saving to file:",e)
        }
        
        return this.getResults()
    }
    /**
     * Returns the results of the spider crawl
     */
    getResults(){
        let data
        if(!Array.isArray(this.data)){
            data = [this.data]
        }
        data = this.data

        return {
            name: this.name,
            links: this.links,
            data: data,
            count: this.data.length,
            lastRun: new Date()
        }
    }
     /** 
   * 
   * @param {*} filepath 
   * @return {Promise<Object>}
   */
    async readJSON(filepath){
        return new Promise((resolve,reject) => { 
            fs.readFile(filepath,(err,data) => {
                if(err){
                    return reject(err)
                }
                return resolve(JSON.parse(data))
            })  
        })
    }
    /**  
     * @return {<String{<year><month><day>}}
     */
    dateString(custom = {}){
        let year = custom.year || new Date().getFullYear() 
        let month = custom.month || (new Date().getMonth()+1) 
        let date =  custom.date || new Date().getDate()
        return (year+"-"+month+"-"+date)
    }
    
    /**
     * 
     */
    async save(path="data/", dataToSave){
        
        let filename = `${path}${this.dateString()}.json`
         
        let data = dataToSave === undefined ? this.getResults() : dataToSave
            let fileExists = async (filename) => {
                let data = {}
                try{
                    data = await this.readJSON(filename)
                }catch(e){
                    console.log("Error Reading JSON File: ",e)
                    data = {}
                }
                    return data
            }
            
            //data = await fileExists(filename)
            //Object.keys(this.data).forEach(key => {
            //    data[key] = this.data[key]
            //})
        return new Promise((resolve,reject) => {
            fs.writeFile(filename, JSON.stringify(data), (err)=>{
                if(err){
                    console.log(err)
                    reject(err)
                }
                resolve("File Finished")
            })
        })
            
      
    }

     formatData(){
       this.data = Scout.convert({data: this.data})
    }
    getData(){
        if(!Array.isArray(this.data)){
            return [this.data]
        }
        return this.data;
    }
    async finish(){
        await this.scout.close()
    }
    onCancel(func,config={function:undefined,path:"data"}){
        let handleExit = async () => {
            console.log("Gracefully close and save progess")
            try{
                if(this.running){
                    await this.save(`downloads`)
                }
                 
            }catch(e){
                console.log(e)
            }
            process.exit()
        }
        if(func !== undefined && typeof(func)=== "function"){
            process.on("SIGINT",func.bind(this))
        }else{
            process.on("SIGINT", handleExit.bind(this))
        }
        
    }
}

module.exports = Spider