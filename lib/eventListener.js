class ListenerTracker{
    constructor(){
        this.isActive= true

        this.elements = []
        this.listeners = []
        this.init()
    }

    init(){
        if(this.isActive){
            return;
        }
        
      this.interceptEvents()
    }
    isTracked(element){
        if(this.elements.indexOf(element) !== -1){
            return false
        }
        return this.listeners[this.elements.indexOf(element)]
    }
    trackElement(element,listener){
        if(this.elements.indexOf(element) === -1){
            
            this.elements.push(element)
            this.listeners.push(listener)

        }
    }
    untrackElement(element,listener){
        if(this.elements.indexOf(element) === -1){
            let index = this.elements.indexOf(element)
            
            this.elements.splice(element,1)
            this.listeners.splice(element,1)
        }
    }
    interceptEvents(){

        let orginal = {
            "addEventListener" : HTMLElement.prototype.addEventListener,
            "removeEventListener" : HTMLElement.prototype.removeEventListener,
        }
        let tracker = this
        let register = this.trackElement.bind(this)
        let deregister = this.untrackElement.bind(this)
        Element.prototype.addEventListener = (type, listener, useCapture) => {
            console.log("Added: ",type," to ",this)
            register(this,{type:type,listener:listener})
            orginal.addEventListener.apply(this,arguments)
        }

        Element.prototype.removeEventListener = (type, listener, useCapture) => {
            console.log("Removed: ",type," to ",this)
            deregister(this,{type:type,listener:listener})
            // add event before to avoid registering if an error is thrown
            original.removeEventListener.apply(this,arguments)
        }

        Element.prototype.getEventListeners = () => {
            //JQUERY
            var clickEvents = this.getStorage().get('prototype_event_registry').get('click');
            clickEvents.each(function(wrapper){
                console.log(wrapper.handler) // alerts "function() { alert('clicked!') }"
            })
            //onclick
            console.log(this.onclick)

            console.log(this.listeners)
            let listener = tracker.isTracked(this)
            if(!listener){
                return listener
            }

        }

    }

}

module.exports = ListenerTracker