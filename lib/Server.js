//server
const Scout = require('./Scout');
const Spider = require('./Spider');
const express = require('express');
const http = require('http');
const socket = require("socket.io");
const cors = require('cors');
const bodyParser = require("body-parser");


class ScoutServer{
    
    constructor(){
        this.app = express()
        this.server = http.Server(this.app)
        this.io = socket(this.server)
        this.browser = new Scout()
        this.spider = new Spider()

        this.defaultServerOptions = {
            headless:false,
            ignoreHTTPSErrors: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
            }
    }

    async init(options={headless:true},port=5000){
        
        let listen = (port) => {
            console.log(`Scout Server Listening on ${port}`)
        } 
        await this.browser.init({
            headless:options.headless,
            ignoreHTTPSErrors: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
            })
        this.app.listen(port)
        //this.server.listen(port,listen(port))
            /*
        this.io.on('connection',(socket) => {

            socket.emit('news',{hello:'world'});

            socket.on('snap',async (data) =>{
                console.log("Screenshot of "+data.url)
                let page = await this.browser.newPage()
                await this.browser.goto(page,data.url)
                let img = await this.browser.snap(page,{fullPage:true}) 
                await page.close()
                socket.emit('snap',{
                    src : "data:image/jpeg;base64,"+img.toString('base64'),
                    blob:img
                })
            })
        })
        */
    }

    async start(){
        //await this.init(this.defaultServerOptions)
        
        let options = {
            index: "index.html"
        }
        console.log(this.browser.wsEndpoint())
        console.log(await this.browser.version())
        let page = await this.newPage()
       //this.app.use(cors)

       this.app.use(bodyParser.json())
       this.app.use(bodyParser.urlencoded({ extended : true}))

        this.app.use('/static',express.static(__dirname+"/admin/build/static"))
        //set server root to build folder 
        //this.app.use('/admin',express.static(__dirname+"/admin/build/"))
        this.app.get('/',(req,res)=>{
            res.sendFile(__dirname+'/admin/public/index.html')
        })
        this.io.on('connection',socket=>{
            socket.emit('snap',{
                src: ""
            })
        })
      
        this.app.get('/snap', async (req, res) => {
            let url = req.query.url
            let action = req.query.action
            
            if(url === undefined) {
                res.send("No url")
                return;
            }
            console.log("Browsing to "+url)

            let page = await this.browser.newPage()
            
            await this.browser.goto(page,url)
            //let options = {path:'one.png'}
            let content = await page.screenshot({fullPage:true})
            await page.close()
            //returns img as base64
            res.send("data:image/jpeg;base64,"+content.toString('base64'))
          
        })
        this.app.get('/fetch',async (req, res) => {
            console.log("Params: " + JSON.stringify(req.params))
            console.log("Query: " + JSON.stringify(req.query))
            let url = req.query.url;
            let action = req.query.action;
            let selectors = [];
            let fetchResponse = {};
            if(req.query.selectors !== undefined)
            if(typeof(req.query.selectors == 'string'))
            selectors = [req.query.selectors]


            if(url === undefined || url.search('http')==-1) {
                res.send("No url")
                return;
            }
               
            console.log("Browsing to "+url)
            let page = await this.browser.newPage()
            let resp = await this.browser.goto(page,url)
             


            //Default send page content
            if(!resp.ok){
                res.send(resp.body)
            }else{

                fetchResponse = await this.browser.getBasic(page)
               if(req.query.og !== undefined){
                   fetchResponse['oGraph'] = await this.browser.getOGraph(page)
               }
                    //selectors
                if(selectors !== undefined && selectors.length > 0){
                   
                    let data = await this.browser.data(page,{selectors: req.query.selectors})
                  
                    fetchResponse['selectors'] = data
                }

               // await this.sanitizeHtml(page)
               fetchResponse['content'] = await page.content()
            }

             
            res.send(fetchResponse)
            await page.close()
               
        })
        this.app.post('/fetch',async (req,res) =>{
            console.log("Body: " + JSON.stringify(req.body))
            let url = req.body.url
          
        
           let selectors = []
           if(typeof(req.body.selectors == 'string'))
           selectors = [req.body.selectors]
            
          
            let page = await this.browser.newPage()
            await this.goto(page,url)
            let result = await this.browser.data(page,{selectors:selectors})
            res.send(result)
           
            await page.close()
        })
        this.app.get('/visited',(req,res)=>{
           
            res.send(this.visited)

            
        })

        this.app.get('/location',async (req,res)=>{
            
            //all of that instead of a simple fetch
            let page = await this.newPage()
            await this.goto(page,"http://ip-api.com/json")
            let json = await this.$(page,'html',t => t.textContent)
            await page.close()
         
            res.send(json)
        })

        

     
    }
}
module.exports = ScoutServer