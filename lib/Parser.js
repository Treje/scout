//let Tesseract = require('tesseract.js'); 
//let natural = require("natural");
class StringParser {
  constructor() {
      this.natural = new natural()
  }
  /**
     * 
     * @param {*} from 
     * @param {*} to 
     */
  static init() {
    if ("getBetween" in String.prototype) {
      return;
    }
    /**@name getBetween - Extracts the text between two strings
             * @param {*} from - text to start from
             * @param {*} to  - text to end at
             */
    Object.defineProperty(String.prototype, "getBetween", {
      /**@name getBetween
             * Extracts the text between two strings
             * @param {*} from - text start from
             * @param {*} to  - text to end at
             */
      value(from, to) {
        let begin = this.indexOf(from) + from.length;
        let end = this.indexOf(to);
        return this.substring(begin, end);
      }
    });
  }

}
class URLParser {
  constructor() {
    this.url = this.urlRegex();
  }
  /**
     * 
     * @param {*} opts 
     */
  urlRegex(opts) {
    var exact = opts && opts.exact !== undefined ? opts.exact : false;
    var ip =
      "(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(?:\\.(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])){3}";
    var protocol = "(?:http(s?)://)?";
    var auth = "(?:\\S+(?::\\S*)?@)?";
    var host = "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)";
    var domain =
      "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*";
    var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))\\.?";
    var port = "(?::\\d{2,5})?";
    var path = '(?:[/?#][^\\s"]*)?';
    var regex =
      "(?:" +
      protocol +
      "|www\\.)" +
      auth +
      "(?:localhost|" +
      ip +
      "|" +
      host +
      domain +
      tld +
      ")" +
      port +
      path;

    return exact
      ? new RegExp("(?:^" + regex + "$)", "i")
      : new RegExp(regex, "ig");
  }
  /**
     * 
     * @param {String} text 
     */
  from(text) {
    let regexp = this.url;
    let matches = [];
    let urls = new Set();

    while ((matches = regexp.exec(text)) !== null) {
      if (matches.index === regexp.lastIndex) {
        regexp.lastIndex++;
      }
      urls.add(matches[0]);
    }

    return urls;
  }
}
class DateParser {
  constructor() {
    this.months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    this.days_of_week = [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ];
    this.tokens = []
    this.year = new Date().getFullYear();
    /*{
        "regex":"",
        "pattern":""
    }*/
    this.expressions = [];
    this.default_value = "N/A";
    this.value = "N/A";
    this.dates = new Set();
    this.init();
  }
  /**
     * 
     * @param {*} options 
     * @param {bool} options.ignoreYear
     * @param {bool} options.shorthandMonth - e.g Dec instead of December
     */
  init(options = {}) {
    let month = "(";
    this.months.map(m => {
      //builds month string with | separating each month
      if (this.months.indexOf(m) == this.months.length - 1) {
        month += `${m})`;
      } else {
        month += `${m}|`;
      }
    });
    let day = "(";
    this.days_of_week.map(d => {
      if (this.days_of_week.indexOf(d) == this.days_of_week.length - 1) {
        day += `${d}) `;
      } else {
        day += `${d}|`;
      }
    });

    //matches day/month/year
    this.addRegex({
      regex: `([0-9]{1,2}).([0-9]{1,2}).([0-9]{4})`,
      pattern: "dd/mm/yyyy"
    });
    //matches year/month/day
    this.addRegex({
      regex: `([0-9]{4}).([0-9]{1,2}).([0-9]{1,2})`,
      pattern: "yyyy/mm/dd"
    });

    this.addRegex({
      regex: `([0-9]{1,2}).([0-9]{1,2}).([0-9]{2})`,
      pattern: "dd/mm/yy"
    });
    this.addRegex({
      regex: `([0-9]{2}).([0-9]{1,2}).([0-9]{1,2})`,
      pattern: "yy/mm/dd"
    });

    //matches day(st/nd/rd/th) Month year and reverse
    this.addRegex({
      regex: `(([0-9]{1,2})([a-zA-Z][a-zA-Z])?.? ${month} ([0-9]{2,4}))`,
      pattern: "dd M yyyy",
      matches: "12th Month 2017"
    });
    this.addRegex({
      regex: `(${month} ([0-9]{1,2})([a-zA-Z][a-zA-Z])?.? ([0-9]{2,4}))`,
      pattern: "M dd yyyy",
      matches: "Month 25th Year"
    });

    // if(options.ignoreYear){
    this.addRegex({
      regex: `${month} ([0-9]{1,2})([a-zA-Z][a-zA-Z])?`,
      pattern: "M dd",
      matches: "Month 25th"
    });
    this.addRegex({
      regex: `([0-9]{1,2})([a-zA-Z][a-zA-Z])?.? ${month}`,
      pattern: "dd M",
      matches: " 23rd Month"
    });
    //}
  }
  setYear(year) {
    this.year = year;
  }
  getYear() {
    return this.year;
  }
  /**
     * 
     * @param {*} text 
     * @param {*} strip_chars 
     */
  cleanString(text, strip_chars = [",", "'"]) {
    return text.replace(",", "");
  }
  addRegex(reg = { regex: "", pattern: "" }) {
    this.expressions.push(reg);
  }
  /**
     * Extracts all dates from a string and return an array
     * @param {string} data 
     * 
     */
  run(data) {
    data = this.cleanString(data);
    //split string into array of sentences
    let lines = [];
    data.split(/\n|\,/).map(line => {
      if (line.trim() !== "") lines.push(line);
    });

    let dates = new Set();
    lines.map(line => {
      this.expressions.map(r => {
        let regexp = new RegExp(r.regex, "g");
        let matches = [];
        try {
          while ((matches = regexp.exec(line)) !== null) {
            if (matches.index === regexp.lastIndex) {
              regexp.lastIndex++;
            }

            let date = {};
            matches.map((match, groupIndex) => {
              // console.log(`Found match, group ${groupIndex}: ${match}`)
            });

            date = this.formatDate(r.pattern, matches);
            if (!this.validate(date)) {
              continue;
            }

            let temp = new Date(`${date.year} ${date.month} ${date.day}`);
            if (temp != "Invalid Date") {
              dates.add(temp.valueOf());

              return;
            }
          }
        } catch (e) {
          this.log(r.pattern, `Error ${e}`);
        }
        //check result
      });
    });
    //console.log(dates)
    return Array.from(dates).map(a => new Date(a));
  }
  formatDate(pattern, matches) {
    let date = {};
    switch (pattern) {
      case "yyyy/mm/dd":
        [, date.year, date.month, date.day] = matches;

        break;
      case "dd/mm/yyyy":
        [, date.day, date.month, date.year] = matches;
        break;
      case "dd M yyyy":
        [, date.day, date.ordinal, date.month, date.year] = matches;
        date.month = this.months.indexOf(date.month) + 1;
        break;
      case "M dd yyyy":
        [, date.month, date.day, date.ordinal, date.year] = matches;
        date.month = this.months.indexOf(date.month) + 1;
        break;
      case "dd M":
        [, date.day, date.ordinal, date.month] = matches;
        date.month = this.months.indexOf(date.month) + 1;
        date.year = this.year;
        break;
      case "M dd":
        [, date.month, date.day, date.ordinal] = matches;
        date.month = this.months.indexOf(date.month) + 1;
        date.year = this.year;
        break;
    }
    date.year = parseInt(date.year,10)
    date.month =  parseInt(date.month,10)
    date.day =  parseInt(date.day,10)
   
    date.context = matches.input;
    return date;
  }
  validate(date) {
    let year = date.year;
    let month = date.month;
    let day = date.day;

    let valid = true;
   
    let daysInMonth = month => new Date(year, month, 0).getDate();
    
    if(day > daysInMonth(month))
        return false
    // Check the ranges of month and year
    if (year < 1000 || year > 2100 || month < 1 || month > 12 || day < 1) 
        return false;

    return valid;
  }
  /**
     * 
     * @param {*} name 
     * @param {*} message 
     */
  log(name, message) {
    console.log(`Pattern ${name}: ${message}`);
  }
}
class IMGParser {
    //consider using Cloud Api ..Google, Amazon ..etc.
    /**
     * 
     */
    constructor(){
        this.Tesseract = Tesseract
    }
    run(image){
       this.Tesseract.recognize(image)
        .progress(message => console.log(message))
        .catch(err => console.error(err))
        .then(result => console.log(result))
        .finally(resultOrError => console.log(resultOrError))
    }
}


module.exports = { DateParser, StringParser, URLParser };
