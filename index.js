module.exports = {
    Scout: require("./lib/Scout"),
    Spider: require("./lib/Spider"),
    Server:  require("./lib/Server")
}