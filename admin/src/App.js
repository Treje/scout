import React, { Component } from 'react';
import SidebarOverlay from './components/Sidebar';
import Form from './components/Form';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import io from 'socket.io-client';

class App extends Component {
constructor(props){
  super(props)
  this.state = {
    img:""
  }
  this.socket = io('http://localhost:5000');
}

  componentDidMount(){
    this.updateNews()
   // (async() => await this.getData())();
  }
  getData = async(url) => {
      if(url === (undefined || "")){
        return -1
      }
     
      let resp = await fetch(`/fetch?url=${url}`)
      console.log(JSON.stringify(resp))
        if(resp.status == 500){
          console.log("error"+ await resp.statusText())
          console.log("error"+ await resp.text())
          return;
        }else{
         
        }
      let body = await resp.text()
      console.log(body)
      this.setState({fetch : body})
     
      

  }
  action = (event) => {
    let target = event.target
    
    let url = document.querySelector('.App #url').value
    this.getData(url).then(error => {
        if(error == -1)
        console.log("No Url")
    });
  }
  updateNews(){
    this.socket.on('news',(data)=>{
        this.setState({news:data})
        this.socket.emit('my other event',{my: 'hi from react'});
    })
  }
  render() {
   
   
    return (
      <Router>
     
      <div className="App">
      <SidebarOverlay/>
      <Switch>
      
      
      </Switch>
      <Route exact path="/" component={Home}/>
      <Route path="/new" component={Form}/>
      <Route path="/screenshot" render={ () => <Screenshot socket={this.socket}/>} />
     
     </div>
    
     </Router>
    );
  }
}
function VideoStream(props){
  console.log(props)
 
  props.socket.emit('video',{url:"http://google.com"})
 
  return (<img src={props.src} width='900' height='700'/>)
}
function Home(props){
  let input = (`<input id="url" placeholder = "Enter a url"/>
  <button id="fetch" onClick={this.action}> Fetch</button>`)
  return ("Welcome to Scout Admin Page")
}

class Screenshot extends Component{
  constructor(props){
    super(props)
    this.state = {images: []}
  }
  
  image(props){
    
    let resize = (event) => {
      let size = '20vw'
      let img = event.target
      img.style.position = null
      img.style.top = null
      img.style.height = size
  
      if(parseInt(img.style.width,10) !== 100){
        size = '100vw'
        img.style.top = '0vh'
        img.style.position = 'absolute'
        img.style.margin = 'auto'
        img.style.height = 'auto'
      }
     
      
      img.style.width = size
    }
  
    return (
        <img onClick={resize} src={props}/>
    )
    
    
  }
  Snaps(props) {
    const imgs = props.imgs
    const list = imgs.map(img => this.image(img))
    return list
  }

  getData = async(url) => {
    if(url === (undefined || "")){
      return -1
    }
    this.props.socket.emit('snap',{url:url})
    this.props.socket.on('snap',data =>{
      this.setState((prevState, props)=>{
        
        images: prevState.images.push(data.src)
      })
      this.setState({
        currentImg : data.src
      })
    })
    this.forceUpdate()
  /*  let resp = await fetch(`/snap?url=${url}`)
    let body = await resp.text()
    this.setState((prevState, props)=>{
      
      images: prevState.images.push(body)
    })*/

   // this.setState({fetch : body})
    
    let img = document.createElement('img')
    img.src = this.state.currentImg //body;
    img.onclick = () => {
      let size = '20vw'
      img.style.position = null
      img.style.top = null
      img.style.height = size

      if(parseInt(img.style.width,10) !== 100){
        size = '100vw'
        img.style.top = '0vh'
        img.style.position = 'absolute'
        img.style.margin = 'auto'
        img.style.height = 'auto'
      }
     
      
      img.style.width = size
      
    }
    document.querySelector('#content').appendChild(img)

  }
  action = (event) => {
    event.preventDefault()
    let target = event.target
    
    let url = document.querySelector('.Screenshot #url').value
    this.getData(url).then(error => {
          if(error == -1)
          console.log("No Url")
    })
    
  }
  render(){
  
   return( <div className="Screenshot">
    <input id="url" placeholder = "Enter a url"/>
    <button id="snap"  onClick={this.action}> Screenshot</button>
    <div id="content"> </div>
    </div>)
  }
}


export default App;
