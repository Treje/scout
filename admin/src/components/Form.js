
import React, {Component} from 'react'


import "./Form.css"
class Form extends Component {
    constructor(props) {
      super(props);
      this.state = {
        value: '',
        labels:[],
        selectors:[],
        rows: [],
        type: 'links' //collect links first then collect data
      };
  
      this.removeRow = this.removeRow.bind(this)
      this.addRow = this.addRow.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
  
      
    }
    componentDidMount(){
        let row = <Row key={0} id={0} onClick={this.removeRow}/>
      
        //instead of pushing create new object
        let newRows = this.state.rows.push(row)
        this.setState((prev,props)=>{
            rows: newRows
        })
        this.setState(this.state)
    }

    removeRow(event){
        let row = event.target.parentNode
       
        let id = row.id.replace(/[^0-9]/g,'')
        let new_rows = []
        this.state.rows.map(a => {
            if(a.key != id)
            new_rows.push(a)
        }) 
        let count = new_rows.length
        this.setState({
            rows:new_rows,
            count:count
        })
    }

    addRow(event){
        let count = this.state.rows.length
        
        let row = <Row key={count} id={count} onClick={this.removeRow}/>
        this.setState((prev,props)=>{
            rows: prev.rows.push(row)
        })
        this.setState({
            rowCount: count
        })
        //this.forceUpdate()
    }
  
    handleSubmit(event) {
      let labels = Array.from(document.querySelectorAll("input[name='label']")).map(a => a.value)
      let selectors = Array.from(document.querySelectorAll("input[name='selector']")).map(a => a.value)
      console.log(labels)
      this.setState({
        labels:labels,
        selectors:selectors
      })
      console.log(this.state)
      console.log('A name was submitted: ' + this.state.value);
      event.preventDefault();
    }
  
    render() {
      
      return (
        <form onSubmit={this.handleSubmit}>
             <i onClick={this.addRow}>+</i>
        <label> Start Url
            <input type="text" placeholder="Start Url"/> 
        </label>
        Disable <input type="checkbox"/>
        <label>Type
            <select>
                <option defaultValue>Links</option>
                <option>Accordion</option>
                <option>Paginate</option>
            </select>
        </label>
        <label> Link Selector
            <input type="text" placeholder="Enter the css selector for the link"/> 
        </label>
        <label> Next Button
            <input type="text" placeholder="Enter css for the paginate next button"/> 
        </label>
        <label> Next Button
            <input type="text" placeholder="Enter text for the button.The element will be searched for and clicked magically"/> 
        </label>

        {this.state.rows.map(a => a)}
         
          
          <input type="submit" value="Submit" />
        </form>
      );
    }
  }

  function Row(props){
    return (<div id={"row"+props.id} className="row">
    <label>
      Label:
      <input name="label" type="text" />
    </label>
    <label>
      Selector:
      <input name="selector" type="text" />
    </label>
    <i onClick={props.onClick}>X</i>
  </div>)
  }

  export default Form;