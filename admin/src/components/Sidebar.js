import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
  } from 'react-router-dom'
  import "./Sidebar.css"
class Sidebar extends Component{
    constructor(props){
        super(props)
        this.toggleSize = this.toggleSize.bind(this)
    }
    toggleSize(event){

        let sidebar = document.querySelector('.Sidebar')
        if(sidebar.classList.contains('collapse')){
            sidebar.classList.remove('collapse')
        }else{
            sidebar.classList.add('collapse')
        }
    }
    render(){
        return (
            <nav className="Sidebar">
                <i onClick={this.toggleSize}>---</i>
                <ul> 
                    <li><Link to="/">Home<i></i></Link></li>
                    <li><Link to="/screenshot">Screenshot</Link></li>
                    <li><Link to="/new">New Form</Link></li>

                </ul>
            </nav>
        )
    }
}

export default Sidebar;