const {Scout,Spider} = require('./index');
const test = require("ava");

let browser = new Scout()

test("Scout.init",async t => {
    await browser.init({headless:true})
    t.is(browser.headless,true,"Browser launched headless")
    await browser.close()
})
test("Convert Parallel Array to Array of Objects",async t =>{
    let testData = {
        letters:['a','b','c','d'],
        colours:['red','blue','orange','green'],
        numbers:[1,2,3,4]
    }
    let expectedData = [ 
        {letters: 'a', colours:'red', numbers: 1 },
        {letters: 'b', colours:'blue', numbers: 2 },
        {letters: 'c', colours: 'orange', numbers: 3 },
        {letters: 'd', colours: 'green', numbers: 4 },
    ]
    let actual = Scout.convert({data:testData})
    t.deepEqual(actual,expectedData)
})

test("Convert Parallel Array to Array of Objects Fill with Padding for Unequal array lengths",async t =>{
    
    let testData = {
        letters:['a','b','c'],
        colours:['red','blue','orange','green'],
        numbers:[1,2,3,4]
    }
    let expectedData = [ 
        {letters: 'a', colours:'red', numbers: 1 },
        {letters: 'b', colours:'blue', numbers: 2 },
        {letters: 'c', colours: 'orange', numbers: 3 },
        {letters: 'no data', colours: 'green', numbers: 4 },
    ]
    let actual = Scout.convert({data:testData,padding:'no data'})
    t.deepEqual(actual,expectedData)
})
