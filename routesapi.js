
let fs = require('fs');
let fetch = require('node-fetch');

class Routes{

    constructor(){
        this.routes = {}
    }

    async getRoutes(){
        let result = await fetch('https://otp.beepbus.com:8081/otp/routers/barbados/index/routes/')
        let routes = await result.json()
        this.routes = routes;
        //fs.writeFileSync(`routes.json`,JSON.stringify(routes))
    }

    async getStopsFor(id){
       let endpoint = `https://otp.beepbus.com:8081/otp/routers/barbados/index/routes/${id}/stops`
       let result = await fetch(endpoint)
       let stops = await result.json()
       return stops;
    }
    async getStops(){
        if(this.routes !== {}){
            this.routes = await Promise.all(this.routes.map(async route => {
                let stops = await this.getStopsFor(route.id)
                route.stops = stops
                fs.writeFileSync(`routes/${route.shortName}.json`,JSON.stringify(route))
                return route
            }))
            fs.writeFileSync(`routes/routes.json`,JSON.stringify(this.routes))
        }
        
    } 
}



(async () => {
    let bbRoutes = new Routes()
    await bbRoutes.getRoutes()
    await bbRoutes.getStops()
})();

