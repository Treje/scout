let { Scout, Spider } = require("../index")
let browser = new Scout()
//let clickEvents = require("./lib/eventListener")
let pad = {
    links:[
        {goto:"http://www.padbds.com/internal-vacancies/"},
        {download:"a[href$='pdf']"},
        {wait:5000},
        {snap:{path:"downloads/test.png",fullPage:true}}
    ],
}

let run = async () => {
    let crawler = new Spider({
        name: "PadBDS",
        scout: browser
        })

        await crawler.scout.init({headless:false})
        crawler.init(pad)
        await crawler.fetch()
        let data = await crawler.getData()
        console.log(JSON.stringify(data))
   
} 

run()



process.on("unhandledRejection", (reason, p) => {
    console.log("Unhandled Rejection at:", p, "reason:", reason)
  });