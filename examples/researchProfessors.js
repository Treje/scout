let {Scout, Spider} = require ('../index');

let browser = new Scout()

let princeton = new Spider({
  name: 'Princeton',
})
let run = async () => {
await princeton.init({
  links:[
    {goto: 'http://www.cs.princeton.edu/people/faculty'},
    {data: {
      labels: [
        "name",
        "title",
        "degree",
        "links",
        "researchInterest"
      ],
      selectors: [
        ".person-name",
        ".person-title",
        ".person-degree",
        ".person-links a",
        ".person-research-interests",
      ]
    }
  },
  ],
  format: true,
})
  await princeton.fetch()
 return princeton.getData()
}

run().then(data => console.log(data))
//cs.princeton.edu/people/faculty

