let {Spider} = require('../index')

class HorribleSubs{
    constructor(){
        this.spider = new Spider({
            name:'HS',
        })
        this.scout = this.spider.scout
        
    }
    async init(){
        await this.scout.init({headless:true})
        this.page = await this.scout.newPage()
        await this.scout.goto(this.page,"http://horriblesubs.info")
    }
    async schedule(){
        if(!this.scout.launched)
            await this.init()

        let schedule = await this.scout.data(this.page,{
            labels:[
                "show",
                "time"
            ],
            selectors:[
                ".schedule-widget-show",
                ".schedule-time"
            ]
        })
        await this.spider.save("schedule.json",schedule)
        return schedule
    }
    async getDownloadLinks(quality){
        let links = await this.page.$$(`*[class$="${quality}p"].release-links`)
        let result = {}

        links = await links.mapLimit(async el =>{ 
            
            let magnet = await el.$('.hs-magnet-link a')
            magnet = await this.page.evaluate(e => e.href ,magnet)
            let torrent = await el.$('.hs-torrent-link a')
            torrent = await this.page.evaluate(e => e.href ,torrent)

            let downloads = await el.$$('.hs-ddl-link a')
            downloads = await downloads.mapLimit(async download => await this.page.evaluate(e =>  {
                if( e.href===undefined){
                    e.href = "N/A"
                }
                let obj = {}
                obj[e.textContent] = e.href
                return obj
            },download))
            let downs = {}
            downloads.forEach(d => {
                let key = Object.keys(d).pop()
                downs[key] = d[key]
            })
            let result = {
                magnet: magnet,
                torrent:torrent,
                direct:downs
            }
            return result
        })
      
        return links
    }
    /**
     * needs fixiing
     * if the first file is missing a link for a quality option 
     * then they are appended tot he wrong object
     */
    async getAllDownloads(){
        //needs fixing
        let links480 = await this.getDownloadLinks('480')
        let links720 = await this.getDownloadLinks('720')
        let links1080 = await this.getDownloadLinks('1080')

        let links = links480.map((val,index)=>{
            return({
                '480p': val,
                '720p': links720[index],
                '1080p':links1080[index]
            })
        })
        return links
    }
    async latest(name="latest"){
        if(!this.scout.launched)
            await this.init()
        let episodes = await this.scout.$$(this.page,'.latest .release-info .rls-label',els => els.map(el => {
            let children = Array.from(el.childNodes).map(node => node.textContent)
            let num = parseInt(children[2].replace("-",""),10)
            return {
                date : children[0],//@TODO convert from (month/day) to date object 
                title: children[1],
                episodeNo: num
            }
         }))

         let links = await this.getAllDownloads()

        episodes = episodes.map((e,i) => {
            return {
                date : e.date,
                title: e.title,
                episodeNo: e.episodeNo,
                links:links[i],
                
            }
        })
        this.spider.save(name,episodes)
        return episodes
    }
    async getShows(){
        if(!this.scout.launched)
            await this.init()
        await this.scout.goto(this.page,"http://horriblesubs.info/shows")
        let shows = await this.scout.$$(this.page,'.linkful a',els => els.map(el => {
            return {
                name:el.textContent,
                href:el.href
            }
        }))
        this.shows = shows
        await this.spider.save('HorribleSubsShows',shows)
        return shows
    }

    async search(title){
        if(this.shows === undefined){
            //await this.spider.readJSON("HorribleSubsShows")
            await this.getShows()
        }
        let found = this.shows.filter(show => show.name.toLowerCase().includes(title.toLowerCase()))
        return found
    }
    async getEpisodes(title){

        let found = await this.search(title).pop()
        await this.scout.goto(this.page,found.href)
        
        let episodes = await this.scout.$$(this.page,'.release-info .rls-label',els => els.map(el => {
            let children = Array.from(el.childNodes).map(node => node.textContent)
            if(Array.from(el.childNodes).length > 1 ){
                return {
                    date : children[0],//@TODO convert from (month/day) to date object 
                    title: children[1],
                    episodeNo: parseInt(children[2],10)
                }
           }else {
               children = children[0]
               let episodeNo = children.split('-').pop()
               let date = /(\d+)\/(\d+)\/(\d+)/.exec(children)[0]
               children = children.replace(`\(${date}\)`,'')
               let title = children.replace(children.substring(children.lastIndexOf('-')),'')
               return {
                date : date,
                title: title,
                episode: episodeNo
            }
        }
    }))
        
        let links = await this.getAllDownloads()
        
                episodes = episodes.map((e,i) => {
                    return {
                        date : e.date,
                        title: e.title,
                        episodeNo: e.episodeNo,
                        links:links[i],
                        
                    }
                })
                this.spider.save(title,episodes)
            
            
            
         
        console.log(episodes)
    }
}
class MAL{
    constructor(){
        this.spider = new Spider({
            name:'MAL',
        })
        this.scout = this.spider.scout   
    }
    async init(){
        await this.scout.init({headless:true})
        this.page = await this.scout.newPage()
        //await this.scout.goto(this.page,"https://myanimelist.net/")
    }

    async search(query){
        await this.scout.goto(this.page,`https://myanimelist.net/anime.php?q=${query}`)
        let searchResults = await this.scout.data(this.page,{
            labels:[
                "title",
                "poster",
                "short_desc",
                "type",
                "Eps",
                "Score"
            ],
            selectors:[
                "strong",
                ".lazyloaded",
                ".pt4",
                ".list table tr:not(:first-child) td:nth-child(3)",
                ".list table tr:not(:first-child) td:nth-child(4)",
                ".list table tr:not(:first-child) td:nth-child(5)",
            ]
        })
        let formatedResults = this.spider.formatData({data:searchResults})
        console.log(formatedResults)
    }

    
}
let m = (async() => {
    
    let HS = new HorribleSubs()
    await HS.init()
    //await HS.getShows()
    let latest = await HS.latest()
    console.log(latest)
    //await HS.search("naruto")
    //await HS.getEpisodes("Sengoku Night Blood")
    await HS.scout.close()
    /*
    let Mal = new MAL()
    await Mal.init()
    await Mal.search("naruto")
    await Mal.scout.close()
    */
})
m().then(()=>{
    console.log("Finished")
})