let { Spider} = require("../index.js");

let wordPlugins  = new Spider()


let run = async () => {
    await wordPlugins.scout.init({headless:true})
    await wordPlugins.init({
        name: "OutdatedPlugins",
        website: "wordpress.com",
        links:[
            {goto: "https://wordpress.org/plugins/browse/popular/page/99/"},
            {paginate: {
                click:"previous",
                query: {
                    labels:[
                        "name",
                        "excerpt",
                        "installs",
                        "tested_with"
                    ],
                    selectors:[
                        "h2.entry-title",
                        ".entry-excerpt",
                        ".active-installs",
                        ".tested-with"
                    ]
                },
                linkSelector : ".entry-title a"
            }},
        ]
    })

    await wordPlugins.fetch()    
    await wordPlugins.save("wordpressPlugins")
}

run()

