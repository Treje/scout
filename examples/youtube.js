let {Spider} = require ('../index');


let getChannelVideos = async (channel) => {
  let youtube  = new Spider()
  await youtube.init({
    name: 'Youtube',
    links: [
      {goto: `http://youtube.com/user/${channel}/videos`},
      {data:  {
        labels: [
        'title',
        'views',
        'uploaded',
        'thumbnail'
      ],
      selectors: [
        '#details #meta #video-title',
        '#details #meta #metadata-line span:first-child',
        '#details #meta #metadata-line span:not(:first-child)',
        '#thumbnail img'
      ]}},
      {format: true},
      {snap:{path:"data/youtube.png",fullPage:true}}
    ],
  })
  await youtube.fetch()
  await youtube.save('youtube');
  return youtube.getData();
  
  }
  let searchChannel = async (channel, query) => {
    let youtube = new Spider();
    youtube.init({
      name: "YoutubeSearchChannel",
      links: [
        {goto: `https://www.youtube.com/user/${channel}/search?query=${query}`},
        {data:  {
          labels: [
          'title',
          'views',
          'uploaded',
          'thumbnail'
        ],
        selectors: [
          '#content #title-wrapper #video-title',
          '#content #metadata-line span:first-child',
          '#content #metadata-line span:not(:first-child)',
          '#thumbnail img'
        ]}},
        {format: true},
      ]
    })
  await youtube.fetch()
  await youtube.save('youtube');
  return youtube.getData();
  }
const search = async (data = {channel: 'movieclipsTRAILERS',search:''} ) => {
  let results = [];
  if(data === undefined){
    throw new Error('No search parameters');
  }
  let channel = data.channel ? data.channel : false;
  let query = data.search ? data.search : false;
  if(channel && query){
    return await searchChannel(channel,query);
  }
  if(channel){
    return await getChannelVideos(channel)
  } 
  if(query){

    
    return;

  }


  
 
}

search({channel: 'movieclipsTRAILERS'}).then( result => {
  console.log(result);
})