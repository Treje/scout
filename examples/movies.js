let {Spider} = require("../index") 

let limegrove = new Spider({website:"http://www.limegrovecinemas.com/index.html"})
let globe = new Spider({website:"http://www.globedrivein.mobi/"})
let olympus = new Spider({website:"http://www.olympus.bb"})


let getTrailerYoutubeUrls = async (/** @type {Scout}*/scout,page) => {
    let page2 = await scout.newPage()
    let urls = await page.$$eval("iframe[src*='youtube']",els => els.map(el => el.src))
    let titles = []
    if(Array.isArray(urls)){
       for(url of urls){
            await page2.goto(url)
            titles.push({title:await page2.title(),trailer:url})
        }
    }   
    await page2.close()
    return titles
}
let getNowShowingGlobe = async () => {
    await globe.scout.init()
    let scout = globe.scout
    let page = await globe.scout.newPage()
    await globe.scout.goto(page,"http://www.globedrivein.mobi/")

    await globe.scout.click(page,"Now Showing")
    let showing = await getTrailerYoutubeUrls(scout,page)

    await globe.scout.click(page,"Next Attraction")
    let next =  await getTrailerYoutubeUrls(scout,page)

    await globe.scout.click(page,"Coming Soon")
    let coming =  await getTrailerYoutubeUrls(scout,page)

    return {
        showing: showing,
        next: next,
        coming: coming
    }
}
let run = async () => {
    let results = {limegrove:{},globe:{},olympus:{}}
    
    results.globe = await getNowShowingGlobe()
    console.log(results.globe)
    await limegrove.scout.init()
    let page = await limegrove.scout.newPage()
    await limegrove.scout.goto(page,"http://www.limegrovecinemas.com/index.html")
    let nowShowing = await page.evaluate(()=>{
       return Array.from(document.querySelectorAll('u')).map(el =>{
            let nowShowing = Array.from(el.parentNode.parentNode.parentNode.childNodes).map(el => el.textContent)
            //rmeove new line character
            nowShowing.shift()
            let title = nowShowing.shift()
            let rating = nowShowing.shift()
            let schedule = nowShowing.map((el,i) => {
                if((i%2==0)&&(i <= nowShowing.length)){
                    return {
                        [el]:nowShowing[i+1]
                    }
                }else{
                    return undefined
                }
            }).filter(el => el !== undefined)
            return {
                title: title,
                rating: rating,
                schedule : schedule,
            }
        })
    })
    console.log(nowShowing)
    let upcoming = await page.$$eval('div div span[style*="0000FF"] strong',els => els.map( el => Array.from(el.childNodes).map(el => el.textContent).filter(el => el !== "") ))

    let comingsoon = []
    let regex = /([0-9]{1,2}) ([a-zA-Z]+) (.*)/
    upcoming[0].map(e => {
      
       let matches = regex.exec(e);
        try{
            [, date,month,title] = matches 
            comingsoon.push({
                date:date,
                month:month,
                title:title
            })
        }catch(e){
            console.log(matches)
        }
      
    
    })    
    results.limegrove.nowShowing = nowShowing
    results.limegrove.comingsoon = comingsoon
    console.log("Upcoming",comingsoon)

}

run().then(() => process.exit(5))