let {Scout, Spider} = require ('../index');


let browser = new Scout()

let run = async () => {
    await browser.init({
        headless:false
    })
    let events = {} 
   // events["myeventsbarbados"] = await myeventsbarbados(browser)
    events["ticketpal"] = await ticketpal()
    //events["ticketnation"] = await ticketNation(browser)
    //events["ticketlinkz"] = await ticketlinkz(browser)
    console.log(events)
    //await browser.close()
}
//TODO eventbrite
/*
let evSpider = new Spider({name:"MyEventsBarbados"})
evSpider.init({
    links: [
        {goto:"https://calendar.myeventsbarbados.com/"},
        {links:"a.timely-event"}
    ],
    data: {
        labels:[
            "title",
            "time",
            "poster",
            "tags",
            "description",
            "venue"
        ],
        selectors:[
            ".timely-event-title",
            ".timely-event-datetime",
            ".timely-featured-image",
            ".timely-event-tags",
            ".timely-event-has-description",
            ".timely-venue-name"
        ]
    }
})
*/


let ticketpal = async () => {
    let tps = new Spider()
    await tps.init({
        name:"TicketPal",
        links:[
            {goto:"http://ticketpal.com"},
            {links:{
                click:"»",
                selector:".eventListInfo > h4 > a:first-child",
            }}
        ],
        data:{
            labels:[
                "title",
                "time",
                "endSaleDate",
                "poster",
                "location",
                "tickets",
                "description",
                "label",
                "price",
            ],
            selectors:[
                ".eventName",
                ".eventTime",
                ".cutoffDate-display",
                ".theImage img",
                ".theVenue",
                ".oneTicketRow",
                ".theDescription",
                ".theTitle",
                ".cart_box"
            ]
        },
        headless:true,
    });
    const { scout } = tps;

    let events = await tps.run()
    await tps.finish()
    return events;
   
    
}
let ticketNation = async (browser) => {
    let page = await browser.newPage()
    await browser.goto(page,"http://ticketnation.nutickets.com")
    let links = await browser.getLinks(page,"tr[id^='eventlist'] td > a")
    let events = links.mapLimit(async link=>{
        let page = await browser.newPage()
        await browser.goto(page,link.href)
        let eventInfo = await browser.data(page,{
            labels:[
                "posters",
                "title",
                "date",
                "location",
                "description"
            ],
            selectors:[
                ".flyer , .event-header .img-responsive",
                ".event-header h3",
                '[class$="event-date"]',
                '[class$="event-location"]',
                "[class$='description']"
            ]
        })
       
        await page.close()
        return eventInfo
    },5)
    return events
}
let ticketlinkz = async () => {
    const spider = new Spider();
    await spider.init({name: 'ticketlinkz', headless: true })

    const { scout } = spider; 
    const browser = scout;

    let page = await browser.newPage()
    await browser.goto(page,"https://ticketlinkz.com/all-events/")
    let links = await browser.getLinks(page,".grid-content h3 a")
    let events = await links.slice(0,1).mapLimit(async link => {
        let eventPage = await browser.newPage()
        await browser.goto(eventPage,link.href)
        let eventInfo = await browser.data(eventPage,{
            labels:[
                "title",
                "poster",
                "posterBackup",
                "startDate",
                "endDate",
                "location"
            ],
            selectors:[
                '.EventNameHeader,.product_title',
                '.zoomImg',
                '.avada-product-gallery-lightbox-trigger',
                '.event-startdate .media-heading',
                '.event-enddate .media-heading',
                '.location-info .media-heading',
            ]
        })
        try{
            const frames = await eventPage.frames();
            await eventPage.waitFor(4000)
            let frame = frames.find(f => f.name() === "MyEventWrapper")
            let info = await frame.$eval('.EventDateLabel',el => el.textContent)
            info = info.split('\n').map(t => t.trim()).filter(t => t !== "");
            eventInfo['event_info'] = info
        }catch(e){
            console.log("iframe failed to load")
            eventInfo['event_info'] = 'No information listed'
        }
    
                
        await eventPage.close()
       
        return eventInfo
    },5)
    spider.links = links;
    spider.data = events;
    await spider.sanitize()
    await spider.save(spider.name, events);
    return events
}
let bimvibes = async(party) =>{
    return [];
    let page = await party.newPage()
   
    await party.goto(page,"http://bimvibes.com/events/upcoming-events")
    let links = await party.getLinks(page,'.concerts-list a')
    let events = await Promise.all(links.map(async (link) => {
        let eventPage = await party.newPage();
        await party.goto(eventPage,link.href)
        let poster = await party.$(eventPage,".event-wrapper .lefthalf img",img => img.src)
        let title = await party.$(eventPage,'.event-wrapper .event-boldtitle',a => a.textContent)
        let info =  await party.$$(eventPage,'.event-wrapper .event-infos-wrap tr',a => a.textContent)
        let desc = await party.$(eventPage,'.event-wrapper .righthalf p',a => a.textContent)
        let [date,time,city,location] = info
        await eventPage.close()
        return {
            title: title,
            poster_link: poster,
            info : info,
            desc : desc,
            date : date,
            time : time,
            city : city,
            location : location
        }
    }));
    return events
}
run()