const {Server} = require('../index');

(async () => {

const browser = new Server()
await browser.initServer({
    headless:false
})

browser.app.post("/whatsapp", (req,res) => {
    if(req.body.reset){
        delete this.qrcode 
    }
    this.qrcode = req.body.qrcode 
})
browser.app.get("/whatsapp",(req,res) => {
    if(this.qrcode)
        res.send(`<img alt="test" src="data:image/jpeg;base64,${this.qrcode}"></img>`)
    else
        res.send("<p>Waiting for QR Code</p>")
})
/*
let page = await browser.newPage()

await page.goto("http://web.whatsapp.com")
await page.waitFor('img')
await page.evaluate(() => {
    //select target node
    let qrCode = document.querySelector("img")
    let clickParent = qrCode.parentNode
    //create an observer instance
    let observer = new MutationObserver(mutations => {
        console.log(mutations)
        mutations.forEach(mutation => {
            //console.log(mutation.type,mutation.target);
            if(mutation.type === "attributes"){
                //console.log(qrCode.src)
            }
        });
        let mutation = mutations.pop()
        console.log(mutation)

        let clone = document.querySelector("#clone")
        if(clone){
            clone =  mutation.target.parentNode.innerHTML
        }else{
            let img = document.createElement("div")
            img.id = "clone"
            img.innerHTML = mutation.target.parentNode.innerHTML
            img.style.margin = "auto"
            img.style.position = "flex"
            img.style.flexFlow = "row"
            document.body.appendChild(img)
        }
        
        
    });
    let clickObserver =  new MutationObserver(mutations => {
        mutations.forEach(mutation => {
          
            if(mutation.type === "childList" || "subtree"){
                console.log(mutation.type,mutation.target);
            }
        });
        
    });
    //configuration of the observer
    let config = {
        childList:true,
        subtree:true,
        attributes:true,
        attributeOldValue:true,
        characterData:true,
        characterDataOldValue:true
    };
    clickObserver.observe(clickParent,config)
    observer.observe(qrCode,config)

    //observer
})
*/
})()