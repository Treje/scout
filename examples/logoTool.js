let {Scout, Spider} = require ('../index');


let browser = new Scout()

let run = async () => {
    await browser.init({headless:false})
    let crawler = new Spider({name:"LogoSpider"})
    let url = process.argv[2]
   
let urlList = [
        "www.google.com",
        "24seveninc.com",
        "abbott.com",
        "advantagecaribbean.com",
        "amicorp.com",
        "automotiveart.com",
        "axcelfinance.com",
        "bcc.edu.bb",
        "biba.bb",
        "bitt.com",
        "blpc.com",
        "caribbeancatalyst.com",
        "caribbeanjobs.com",
        "ccree.org",
        "centralbank.org",
        "chefette.com",
        "cwc.com",
        "digicel.com",
        "faviogroup.com",
        "https://www.cibc.com/fcib/"
]

   let info = await urlList.slice(0,1).mapLimit(async url => {
        url = `http://${url}`
        let page = await browser.newPage()
        let logos = []
        let isLogoInRequest = (request) => {

            if(/logo/i.test(request.url) && request.resourceType === "image"){ 
                //console.log("Request To => ",request.url)
                logos.push(request.url)
            }
        }
        await page.on("request",async request => {
               isLogoInRequest(request)
         })
        logos = Array.from(new Set(logos))
        await page._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: './'})
        await browser.goto(page,url)
        let info = {}
      
        let imgLogo = await browser.getLogo(page)
        
       // info["logo"] = imgLogo
        console.log(logos,imgLogo)
        await page.close()

        return {url:[imgLogo,logos]}
    },5)
   /*
    let page = await browser.newPage()
    let logos = []
    await page.on("request",async request => {
       
        if(/logo/.test(request.url)&& request.resourceType === "image")
           { console.log("Request To => ",request.url)
            logos.push(request.url)
        }
    })
    logos = Array.from(new Set(logos))
   // await page._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: './'})
    await browser.goto(page,url)
   // console.log(await browser.getBasic(page))
  //  let logos = await browser.$$(page,"[id$='logo'],[class$='logo']",a => a.innerHTML)
   
    let imgLogo = await browser.getLogo(page)
*/
console.log(info)
  //  console.log(info,imgLogo,logos)
}

run()